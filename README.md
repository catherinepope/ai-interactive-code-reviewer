## Interactive Code Reviewer

This Python script uses the OpenAI API to create an interactive code reviewer. Pass the filename as an argument, and the bot suggests changes. You can either accept or reject them, or ask a question about the suggestion.

You need to add your API key as an `.env` file.

Based on a project from the [Mastering OpenAI Python APIs course](https://www.udemy.com/course/mastering-openai/).
